import axios from 'axios'
import { reactive } from 'vue'
const BASE_URL = process.env.VUE_APP_BASEURL
const url = `${BASE_URL}/registration`
interface stateObject {
  [key: string]: any
}
const state: stateObject = reactive({})
const actions = {
  //Получаем данные и отправляем на серве для регистрации
  getData<T extends { toString: () => string }>(user: T) {
    //Запрос на сервер для регистрации
    axios
      .post(url, user)
      .then((res) => {
        state.checkStatus = res.status //Получаем статус
        console.log(res.status)

        state.isReg = !res.data.exist
      })
      .catch((e) => console.log(e))
  },
}

export default { ...actions, state }
